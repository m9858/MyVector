#include "Vector.h"
#include <iostream>

void printVector(Vector& other){
	std::cout << "Vector output" << std::endl;
	std::cout << other.size() << std::endl;
	for (size_t i = 0; i < other.size(); i++){
		std::cout << other[i] << " ";
	}
	std::cout << std::endl;
}


int main(){
	double* a = new double[2];
	a[0] = 200;
	a[1] = 210;
	std::cout << "Сreating our own vector" << std::endl;
	Vector a1;

	std::cout << "Adding elements in the vector" << std::endl;
	a1.pushBack(102);
	a1.pushFront(111);
	a1.insert(110, 1);
	a1.insert(a, 2, 3);
	for (int i = 0; i < 1000; i++){
		a1.pushBack(i + 100);
	}
	printVector(a1);

	std::cout << "Deleting elements" << std::endl;
	a1.popBack();
	a1.popFront();
	a1.erase(1, 4);
	printVector(a1);
	a1.eraseBetween(5, 8);
	printVector(a1);

	
	std::cout << "Returns load factor: " << a1.loadFactor() << std::endl;
	std::cout << (a1.find(19) == -1 ? "Element not found" : "Element found") << std::endl;
	std::cout << (a1.find(108) == -1 ? "Element not found" : "Element found") << std::endl;

	std::cout << "Increase memory allocation of space by 100 items" << std::endl;
	a1.reserve(100);
	std::cout << a1.capacity() << std::endl;

	std::cout << "The size of the vector leads to the size of the memory" << std::endl;
	a1.shrinkToFit();
	std::cout << a1.capacity() << std::endl;

	Vector::Iterator as = a1.begin();
	std::cout << *as << std::endl;
	as++;
	std::cout << *as << std::endl;

	delete[] a;


	return 0;
}
