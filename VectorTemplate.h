#pragma once

#include <iostream>
#include <cmath>
#include <stdexcept>


template <class Value>
class Vector{
	public:
	    Vector() = default;
	    Vector(const Value* rawArray, const size_t size, float coef = 2.0f);

	    explicit Vector(const Vector<Value>& other);
	    Vector& operator=(const Vector<Value>& other);

	    explicit Vector(Vector<Value>&& other) noexcept;
	    Vector& operator=(Vector<Value>&& other) noexcept;

	    ~Vector();

	    void pushBack(const Value& value);
	    void pushFront(const Value& value);
	    void insert(const Value& value, size_t pos);
	    void insert(const Value* values, size_t size, size_t pos);
	    void insert(const Vector<Value>& vector, size_t pos);

	    void popBack();
	    void popFront();
	    void erase(size_t pos, size_t count = 1);
	    void eraseBetween(size_t beginPos, size_t endPos);

	   
	    size_t size() const;
	    size_t capacity() const; 
	    double loadFactor() const;

	    Value& operator[](size_t idx);
	    const Value& operator[](size_t idx) const;
	    long long find(const Value& value) const;
	    void reserve(size_t capacity);
	    void shrinkToFit();
	    class Iterator{
		private:
			Value* _ptr;
	   	public:
			explicit Iterator(Value* ptr);
		    
			Value& operator*();
		    
			const Value& operator*() const;
		    
			Value* operator->();
		    
			const Value* operator->() const;
		    
			Iterator operator++();
		    
			Iterator operator++(int);
		    
			bool operator==(const Iterator& other) const;
		    
			bool operator!=(const Iterator& other) const;
	    };
	    Iterator begin();
	    Iterator end();
	private:
	    Value* _data = nullptr;
	    size_t _size = 0;
	    size_t _capacity = 0;
	    float _multiplicativeCoef = 2.0f;
	    void _reallocNew(size_t newSize);
};

template<class Value>
void Vector<Value>::_reallocNew(size_t newSize) {
	if ( !this-> _capacity ) {
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if ( newSize > this-> _capacity ) {
		while (newSize > this-> _capacity ) {
			this->_capacity *= this->_multiplicativeCoef;
		}
		Value* newData = new Value[this->_capacity];
		for (size_t i = 0; i < this->_size; i++) {
			newData[i] = this->_data[i];
		} 
		delete this->_data;
		this->_data = newData;
	}
}

template<class Value>
Vector<Value>::Vector(const Value* rawArray, const size_t size, float coef){
	this->_size = size;
	this->_capacity = size;
	this->_multiplicativeCoef = coef;
	this->_data = new Value[_capacity];
	for(size_t i = 0; i < size; i++){
		this->_data[i] = rawArray[i];
	}
}

template<class Value>
Vector<Value>::Vector(const Vector<Value> & other){
	*this = other;
}

template<class Value>
Vector<Value>& Vector<Value>::operator=(const Vector<Value>& other){
	if (this == &other){
		return *this;
	}
	this->~Vector();
	this->_size = other._size;
	this->_capacity = other._size;
	this->_multiplicativeCoef = other._multiplicativeCoef;
	this->_data = new Value[other._size];
	for (size_t i = 0; i < this->_size; i++){
		this->_data[i] = other._data[i];
	}
	return *this;
}

template<class Value>
Vector<Value>::Vector(Vector<Value>&& other) noexcept{
	if (this == &other){
		return;
	}
	this->_data = other._data;
	other._data = nullptr;
	this->_size = other._size;
	this->_capacity = other._capacity;
	other._capacity = 0;
	other._size = 0;
	this->_multiplicativeCoef = other._multiplicativeCoef;
}

template<class Value>
Vector<Value>& Vector<Value>::operator=(Vector<Value>&& other) noexcept{
	if (this == &other){
		return *this;
	}
	this->_data = other._data;
	other._data = nullptr;
	this->_size = other._size;
	this->_capacity = other._capacity;
	other._capacity = 0;
	other._size = 0;
	this->_multiplicativeCoef = other._multiplicativeCoef;
	return *this;
}

template<class Value>
Vector<Value>::~Vector() { 
	for (size_t i = 0; i < this->_size; i++){
		this->_data[i].~Value();
	}
	delete[] this->_data;
	this->_data = nullptr;
	this->_size = 0;
	this->_capacity = 0;
}

template<class Value>
void Vector<Value>::pushBack(const Value& value){
	if (!this->_capacity){
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if (this->_size + 1 > this->_capacity){
		this->_capacity = this->_capacity * this->_multiplicativeCoef;
		Value* tmp = new Value[this->_capacity];
		for(size_t i = 0; i < this->_size; i++){
			tmp[i] = this->_data[i];
		}
		delete[] this->_data;
		this->_data = tmp;
	}
	this->_data[this->_size++] = value;
}

template<class Value>
void Vector<Value>::pushFront(const Value& value){
	if (!this->_capacity){
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if (this->_size + 1 > this->_capacity){
		this->_capacity = this->_capacity * this->_multiplicativeCoef;
		Value* tmp = new Value[this->_capacity];
		for(size_t i = 0; i < this->_size; i++){
			tmp[i] = this->_data[i];
		}
		delete[] this->_data;
		this->_data = tmp;
	}
	for (size_t i = this->_size; i > 0 ; i--){
		this->_data[i] = this->_data[i - 1];
	}
	this->_data[0] = value;
	this->_size += 1;
}

template<class Value>
void Vector<Value>::insert(const Value& value, size_t pos){
	if (this->_size < pos){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	if (!this->_capacity){
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if (this->_size + 1 > this->_capacity){
		this->_capacity =  this->_capacity * this->_multiplicativeCoef;
		Value* tmp = new Value[this->_capacity]; 
		for (size_t i = 0; i < this->_size; i++){
			tmp[i] = this->_data[i];
		}
		delete[] this->_data;
		this->_data = tmp;
	}
	for (size_t i = this->_size; i > pos; i--){
		this->_data[i] = this->_data[i - 1];
	}
	this->_data[pos] = value;
	this->_size++;
}

template<class Value>
void Vector<Value>::insert(const Value* values, size_t size, size_t pos){
	if (this->_size < pos){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	if (!this->_capacity){
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if (this->_size + size > this->_capacity){
		while(this->_size + size > this->_capacity){
			this->_capacity = this->_capacity * this->_multiplicativeCoef;
		}
		Value* tmp = new Value[this->_capacity]; 
		for (size_t i = 0; i < this->_size; i++){
			tmp[i] = this->_data[i];
		}
		delete[] this->_data;
		this->_data = tmp;
	}
	for (size_t i = this->_size - 1; i >= pos; i--){
		this->_data[size + i] = this->_data[i];
	}
	for (size_t i = 0; i < size; i++){
		this->_data[i + pos] = values[i];
	}	
	this->_size += size;
}

template<class Value>
void Vector<Value>::insert(const Vector<Value>& vector, size_t pos){
	if (this->_size < pos){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	this->insert(vector._data, vector._size, pos);
}

template<class Value>
void Vector<Value>::popFront(){
	if (!this->_size){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	for (size_t i = 0; i < this->_size - 1; i++){
		this->_data[i] = this->_data[i + 1];
	}
	this->_size--;
}

template<class Value>
void Vector<Value>::popBack(){
	if (!this->_size){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	this->_size--;
}

template<class Value>
void Vector<Value>::erase(size_t pos, size_t count){
        if (this->_size < pos){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	size_t erase_count = std::min(count, this->_size - pos);
	for (size_t  i = pos; i < this->_size - erase_count;  i++){
		this->_data[i] = this->_data[erase_count + i];
	}
	this->_size -= erase_count;
} 

template<class Value>
void Vector<Value>::eraseBetween(size_t beginPos, size_t endPos){
	if (this->_size < endPos - beginPos){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	erase(beginPos, endPos - beginPos);
}

template<class Value>
size_t Vector<Value>::size() const{
	return this->_size;
}

template<class Value>
size_t Vector<Value>::capacity() const{
	return this->_capacity;
}

template<class Value>
double Vector<Value>::loadFactor() const{
       	return static_cast<double>(this->_size) / this->_capacity;
}

template<class Value>
Value& Vector<Value>::operator[](size_t idx){
	std::cout << idx << std::endl;
	if (this->_size <= idx){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	return this->_data[idx];
}

template<class Value>
const Value& Vector<Value>::operator[](size_t idx) const{
	if (this->_size <= idx){
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	return this->_data[idx];
}

template<class Value>
long long Vector<Value>::find(const Value& value) const{
	for (size_t i = 0; i < this->_size; i++){
		if (this->_data[i] == value){
			return i;
		}
	}
	return -1;
}

template<class Value>
void Vector<Value>::reserve(size_t capacity){
	if (capacity <= this->_capacity){
		return;
	}
	Value* tmp = new Value[capacity];
	for (size_t i = 0; i < this->_size; i++){
		tmp[i] = this->_data[i];
	}
	delete[] this->_data;
	this->_data = tmp;
	this->_capacity = capacity;
}

template<class Value>
void Vector<Value>::shrinkToFit(){
	if (this->_size == this->_capacity){
		return;
	}
	if (this->_size == 0){
		this->~Vector();
		return;
	}
	Value* tmp = new Value[this->_size];
	for (size_t i = 0; i < this->_size; i++){
		tmp[i] = this->_data[i];
	}
	delete[] this->_data;
	this->_data = tmp;
	this->_capacity = this->_size;
}

template<class Value>
Vector<Value>::Iterator::Iterator(Value* ptr){
       this->_ptr = ptr;
}

template<class Value>
Value& Vector<Value>::Iterator::operator*(){
	return *this->_ptr;
}

template<class Value>
const Value& Vector<Value>::Iterator::operator*() const{
	return *this->_ptr;
}

template<class Value>
typename Vector<Value>::Iterator Vector<Value>::Iterator::operator++(){
	this->_ptr++;
	return *this;
}

template<class Value>
typename Vector<Value>::Iterator Vector<Value>::Iterator::operator++(int){
	Vector::Iterator tmp(this->_ptr);
	this->_ptr++;
	return tmp;
}

template<class Value>
Value* Vector<Value>::Iterator::operator->(){
	return this->_ptr;
}

template<class Value>
const Value* Vector<Value>::Iterator::operator->() const{
	return this->_ptr;
}

template<class Value>
bool Vector<Value>::Iterator::operator==(const Vector<Value>::Iterator& other) const{
	return this->_ptr == other._ptr;
}

template<class Value>
bool Vector<Value>::Iterator::operator!=(const Vector<Value>::Iterator& other) const{
	return this->_ptr != other._ptr;
}

template<class Value>
typename Vector<Value>::Iterator Vector<Value>::begin(){
	Vector<Value>::Iterator tmp(this->_data);
	return tmp;
}

template<class Value>
typename Vector<Value>::Iterator Vector<Value>::end(){
	Vector<Value>::Iterator tmp(this->_data + this->_size);
	return tmp;
}
