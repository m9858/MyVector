.PHONY: all clear run

all: main_test

main_test: Vector.o main_test.o
	g++ $^ -o main_test -std=c++17

%.o: %.cpp
	g++ -c $< -o $*.o  -std=c++17

clean:
	rm -f *.o main_test onput exhaust_valgirng 

run:main_test 
	valgrind ./main_test 2> exhaust_valgirng > onput
