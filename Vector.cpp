#include "Vector.h"

#include <cmath>
#include <stdexcept>


//Если нельзя менять заголовочник то так выделяем память
static void reallocNew(Value* (&other), size_t& capasit, size_t size, size_t newSize, float coef) {
	if ( !capasit ) {
		other = new Value[static_cast<size_t>(coef)];
		capasit = coef;
	}
	size_t newCapasit = capasit;
	if ( newSize > newCapasit ) {
		while (newSize > newCapasit) {
			newCapasit *= coef;
		}
		Value* newOther = new Value[newCapasit];
		for (size_t i = 0; i < size; i++) {
			newOther[i] = other[i];
		}
		delete other;
		other = newOther;
		capasit = newCapasit;
	}
}

// Если  можно менять заголовчник то так
void Vector::_reallocNew(size_t newSize) {
	if ( !this-> _capacity ) {
		this->_data = new Value[static_cast<size_t>(this->_multiplicativeCoef)];
		this->_capacity = this->_multiplicativeCoef;
	}
	if ( newSize > this-> _capacity ) {
		while (newSize > this-> _capacity ) {
			this->_capacity *= this->_multiplicativeCoef;
		}
		Value* newData = new Value[this->_capacity];
		for (size_t i = 0; i < this->_size; i++) {
			newData[i] = this->_data[i];
		} 
		delete this->_data;
		this->_data = newData;
	}
}

Vector::Vector(const Value* rawArray, const size_t size, float coef) {
	this->_size = size;
	this->_capacity = size;
	this->_multiplicativeCoef = coef;
	this->_data = new Value[_capacity];
	for(size_t i = 0; i < size; i++) {
		this->_data[i] = rawArray[i];
	}
}

Vector::Vector(const Vector & other) {
	*this = other;
}

Vector& Vector::operator=(const Vector& other) {
	if ( this == &other ) {
		return *this;
	}
	this->~Vector();
	this->_size = other._size;
	this->_capacity = other._size;
	this->_multiplicativeCoef = other._multiplicativeCoef;
	this->_data = new Value[other._size];
	for (size_t i = 0; i < this->_size; i++) {
		this->_data[i] = other._data[i];
	}
	return *this;
}


Vector::Vector(Vector&& other) noexcept{
	if ( this == &other ) {
		return;
	}
	this->_data = other._data;
	other._data = nullptr;
	this->_size = other._size;
	this->_capacity = other._capacity;
	other._capacity = 0;
	other._size = 0;
	this->_multiplicativeCoef = other._multiplicativeCoef;
}

Vector& Vector::operator=(Vector&& other) noexcept{
	if ( this == &other ) {
		return *this;
	}
	this->_data = other._data;
	other._data = nullptr;
	this->_size = other._size;
	this->_capacity = other._capacity;
	other._capacity = 0;
	other._size = 0;
	this->_multiplicativeCoef = other._multiplicativeCoef;
	return *this;
}


Vector::~Vector() {
	delete[] this->_data;
	this->_data = nullptr;
	this->_size = 0;
	this->_capacity = 0;
}


void Vector::pushBack(const Value& value) {
	/* Если нельзя изментять заголовчник то так 
	reallocNew(this->_data, this->_capacity, this->_size, 
			(this->_size + 1), this->_multiplicativeCoef);*/
	this->_reallocNew( (this->_size + 1) );
	this->_data[this->_size++] = value;
}


void Vector::pushFront(const Value& value) {
	/*Если нельзя изментять заголовчник то так 
	reallocNew(this->_data, this->_capacity, this->_size,
		       	(this->_size + 1), this->_multiplicativeCoef);*/ 
	this->_reallocNew( (this->_size + 1) );
	for (size_t i = this->_size; i > 0 ; i--) {
		this->_data[i] = this->_data[i - 1];
	}
	this->_data[0] = value;
	this->_size++;
}


void Vector::insert(const Value& value, size_t pos) {
	if ( this->_size < pos ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	/*Если нельзя изментять заголовчник то так 
	reallocNew(this->_data, this->_capacity, this->_size,
			(this->_size + 1), this->_multiplicativeCoef);*/
	this->_reallocNew( (this->_size + 1) );
	for (size_t i = this->_size; i > pos; i--) {
		this->_data[i] = this->_data[i - 1];
	}
	this->_data[pos] = value;
	this->_size++;
}


void Vector::insert(const Value* values, size_t size, size_t pos) {
	if ( this->_size < pos ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	/* Если нельзя изментять заголовчник то так 
	reallocNew(this->_data, this->_capacity, this->_size,
			(this->_size + size), this->_multiplicativeCoef);*/ 
	this->_reallocNew( (this->_size + size) );
	for (size_t i = this->_size - 1; i >= pos; i--) {
		this->_data[size + i] = this->_data[i];
	}
	for (size_t i = 0; i < size; i++) {
		this->_data[i + pos] = values[i];
	}	
	this->_size += size;
}


void Vector::insert(const Vector& vector, size_t pos) {
	if ( this->_size < pos ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	this->insert(vector._data, vector._size, pos);
}


void Vector::popFront() {
	if ( !this->_size ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	for (size_t i = 0; i < this->_size - 1; i++) {
		this->_data[i] = this->_data[i + 1];
	}
	this->_size--;
}


void Vector::popBack() {
	if ( !this->_size ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	this->_size--;
}


void Vector::erase(size_t pos, size_t count) {
        if ( this->_size < pos ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	size_t erase_count = std::min(count, this->_size - pos);
	for (size_t  i = pos; i < (this->_size - erase_count);  i++) {
		this->_data[i] = this->_data[erase_count + i];
	}
	this->_size -= erase_count;
} 


void Vector::eraseBetween(size_t beginPos, size_t endPos) {
	if ( this->_size < (endPos - beginPos) ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	erase(beginPos, (endPos - beginPos));
}


size_t Vector::size() const{
	return this->_size;
}


size_t Vector::capacity() const{
	return this->_capacity;
}


double Vector::loadFactor() const{
       	return (static_cast<double>(this->_size) / this->_capacity);
}


Value& Vector::operator[](size_t idx) {
	if ( this->_size < idx ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	return this->_data[idx];
}


const Value& Vector::operator[](size_t idx) const{
	if ( this->_size < idx ) {
		throw std::invalid_argument("Error: going beyond the edges of the array");
	}
	return this->_data[idx];
}


long long Vector::find(const Value& value) const{
	for (size_t i = 0; i < this->_size; i++) {
		if ( this->_data[i] == value ) {
			return i;
		}
	}
	return -1;
}


void Vector::reserve(size_t capacity) {
	if ( capacity <= this->_capacity ) {
		return;
	}
	Value* tmp = new Value[capacity];
	for (size_t i = 0; i < this->_size; i++) {
		tmp[i] = this->_data[i];
	}
	delete[] this->_data;
	this->_data = tmp;
	this->_capacity = capacity;
}


void Vector::shrinkToFit() {
	if ( this->_size == this->_capacity ) {
		return;
	}
	if ( this->_size == 0 ) {
		this->~Vector();
		return;
	}
	Value* tmp = new Value[this->_size];
	for (size_t i = 0; i < this->_size; i++) {
		tmp[i] = this->_data[i];
	}
	delete[] this->_data;
	this->_data = tmp;
	this->_capacity = this->_size;
}


Vector::Iterator::Iterator(Value* ptr) {
       this->_ptr = ptr;
}


Value& Vector::Iterator::operator*() {
	return *this->_ptr;
}


const Value& Vector::Iterator::operator*() const{
	return *this->_ptr;
}


Vector::Iterator Vector::Iterator::operator++() {
	this->_ptr++;
	return *this;
}


Vector::Iterator Vector::Iterator::operator++(int) {
	Vector::Iterator tmp(this->_ptr);
	this->_ptr++;
	return tmp;
}


Value* Vector::Iterator::operator->() {
	return this->_ptr;
}


const Value* Vector::Iterator::operator->() const{
	return this->_ptr;
}


bool Vector::Iterator::operator==(const Vector::Iterator& other) const{
	return this->_ptr == other._ptr;
}


bool Vector::Iterator::operator!=(const Vector::Iterator& other) const{
	return this->_ptr != other._ptr;
}


Vector::Iterator Vector::begin() {
	Vector::Iterator tmp(this->_data);
	return tmp;
}


Vector::Iterator Vector::end() {
	Vector::Iterator tmp( (this->_data + this->_size) );
	return tmp;
}
